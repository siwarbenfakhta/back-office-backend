<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App ;
App::bind('App\Repository\UserRepositoryInterface');
App::bind('App\Repository\Eloquent\UserRepository');

Route::get('/', function () {
    return view('welcome');
});




//Product
Route::get('/product','ProductController@getAll');
Route::get('/getbyid/{id}','ProductController@getProductbyId');
Route::post('/addProduct','ProductController@addProduct');
Route::delete('/deleteProduct/{id}','ProductController@deleteProduct');
Route::patch('/updateProduct/{id}','ProductController@updateProduct');
Route::get('/verify/{token}' , 'verifyController@verify');

//  User
Route::post('/SignUp','UserController@SignUp');
Route::post('/login','UserController@login');
Route::get('/users','UserController@getAll');
Route::delete('/deleteUser/{id}','UserController@deleteUser');

