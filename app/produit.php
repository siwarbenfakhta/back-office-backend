<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class produit extends Model
{
    protected $table = 'produits';
    protected $primaryKey = 'id';
    protected $fillable   = ['label', 'prix', 'quantite'];
    public $timestamps = false;
    public function produit()
{
    return $this->hasMany(produit::class, 'id');
}


}
