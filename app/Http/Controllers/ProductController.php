<?php
namespace App\Http\Controllers;
use App\produit;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\VarDumper\Cloner\Data;
use App\Repository\ProduitRepositoryInterface;

class ProductController extends Controller
{private $produitRepository;
  
    public function __construct(ProduitRepositoryInterface $produitRepository)
    {
        $this->produitRepository = $produitRepository;
    }
    //Afficher tous les produits
    public function getAll()
    {
        $produits = $this->produitRepository->all();
        return response()-> json($produits);
        ;
    }
 
    //Afficherun seul produit
    public function getProductbyId(Request $request){
        $id = $request->id ;
        $produit = $this->produitRepository->getProductbyId($id);
        return response()-> json($produit);
    }

    
    //Ajouter un produit
    public function addProduct (Request $request ){
        $data = $request->all();
        $this->produitRepository->addProduct($data);
    }

  //Mettre à jour un produit
    public function updateProduct(Request $request){
            $id = $request->id ;
            $data = $request->all();
            $this->produitRepository->updateProduct($id ,$data);
       }

    //Supprimer un produit
    public function deleteProduct (Request $request){
        $id = $request->id ; 
        $this->produitRepository->deleteProduct($id);
    }

    



 
}