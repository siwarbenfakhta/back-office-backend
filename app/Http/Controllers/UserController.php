<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function SignUp(Request $request)
    { 
         $this->validate($request, [
        'email' => 'required|email|unique:users',
    ]); 
         
        $request['password'] = Hash::make($request->password);
        $UserModel= new User() ;
        $UserData = $UserModel->getUser($request->email); 
        //vérifier l'unicité de l'email
            $data = $UserModel ->SignUp($request->all());
            $response['data'] = $request->all();
            return response()->json($response); 

}

    public function login(Request $request){
         $UserModel= new User() ;
        $email = $request->email ;
        $password = $request->password;
        $UserData = $UserModel->login($email, $password); 
        if($UserData){
            $input = $request->only('email', 'password');
            $token = JWTAuth::attempt($input);
            $response['token'] = $token;
            $reponse['status'] = 1;
            $response['message']="login success";
            
        }
        else {
            $response['message']="password is wrong"; 
            $response['status']= 0;       }
            return response()->json($response);
}

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function getAccount()
    {
        return view('account', ['user' => Auth::user()]);
    }

    

    public function getUserImage($filename)
    {
        $file = Storage::disk('local')->get($filename);
        return new Response($file, 200);
    }

    public function getAll(){
        $UserModel= new User ;
        $Data = $UserModel ->  getAll();  
        return response()-> json($Data);
        }

        public function deleteUser(Request $request){
            $id = $request->id ;
            $UserModel= new User ;
            $Data = $UserModel ->  deleteUser($id);  
        }

}