<?php
namespace App;
use Illuminate\Auth\Notifications\VerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

   /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    protected $table = 'users';
    public $timestamps = false;
    protected $primaryKey = 'id';
    use Notifiable;
    protected $fillable = [
        'nom','prenom', 'email', 'password','is_activated','token'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    //USER REGISTRATION
    public function SignUp($data)
    {
     DB::table("users")-> insert($data);
    } 
    

    public function getAll(){
        //echo"product called";
        $data = DB::table("users")-> get();
        return $data ; 
    }

    //GET USER BY EMAIL
    public function getUser($email){
        $data = DB::table('users')->where('email', $email)->get()->first();
        return $data ;
    }

    
    public function Login($email , $password){
        $data = DB::table('users')->where('email', $email)->get()->first();
        if (Hash::check($password,$data->password)){
            return $data;
        }}

        public function deleteUser($id){
            DB::table("users")-> where('id' , $id)->delete();
             
         }







}
