<?php

namespace App\Repository\Eloquent;

use App\produit;
use App\Repository\ProduitRepositoryInterface;
use Illuminate\Support\Collection;

class ProduitRepository extends BaseRepository implements ProduitRepositoryInterface
{

   /**
    * produitRepository constructor.
    *
    * @param produit $model
    */
   public function __construct(produit $model)
   {
       parent::__construct($model);
   }

   /**
    * @return Collection
    */
   public function all(): Collection
   {
       return $this->model->all();    
   }
   public function getProductbyId($id)
   {
       return $this->model->where('id', $id)->get()->first();   
   }
   public function deleteProduct($id){
       return $this->model->where('id' , $id)->delete();
   }
   public function addProduct($data){
       return $this->model-> insert($data);
   }
   public function updateProduct($id ,$data){
       return $this->model-> where('id' , $id)->update($data);
   }

}