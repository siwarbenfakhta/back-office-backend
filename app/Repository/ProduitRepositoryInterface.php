<?php
namespace App\Repository;

use App\produit;
use Illuminate\Support\Collection;

interface ProduitRepositoryInterface
{
   public function all(): Collection;
   public function getProductbyId($id);
   public function deleteProduct($id);
   public function addProduct($data);
   public function updateProduct($id ,$data);
}